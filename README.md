## Free Security eBooks from PacktPub

The repository is a fork of the original repository : [`free-tech-ebooks-from-packtpub`](https://github.com/tahmid-choyon/free-tech-ebooks-from-packtpub) which is maintained by [**tahmid-choyon**](https://github.com/tahmid-choyon).

#### Original Readme:
> Free Technology Ebooks from [Packt Publishing](https://www.packtpub.com/).
>
> **Packt Publishing** offers a free technology ebook a day. You can avail this [here](https://www.packtpub.com/packt/offers/free-learning).
>
> All you have to do is to visit that URL and claim you free book. Additionally after you add books in your account you can read it online, download PDF, EPUB and MOBI version and send to your Kindle.
>
> In case you missed some of those cool ebooks, I have gathered some from my collection and will be updating regularly.

However this repository is exclusively for security folks and is a part of *Hack with GitHub* repository: [Free Security eBooks](https://github.com/Hack-with-Github/Free-Security-eBooks).

The unwanted eBooks were deleted in the commit [63a9995](https://github.com/Hack-with-Github/Free-Security-eBooks-from-PacktPub/commit/63a999555a0bb567da1ef94f8c1bc8a324ac553d) but still the removed books do exist in the version control history of this repository.

**Please don't clone this repository as the size is nearly 1 GB. Instead click on the required book and then save it.**

If are interested in eBooks related to programming, development and much more, you might find a relevant book in the [original repository](https://github.com/tahmid-choyon/free-tech-ebooks-from-packtpub).

### FAQ

**1. Hosting free ebooks. Is it legal ?**

I would like to say it again, *it is just a fork of another repository*. It seems [**tahmid-choyon**](https://github.com/tahmid-choyon) has signed up for receiving these free ebooks and he is uploading his free ebooks to his GitHub repository.

Hack with GitHub just pulls the latest ebooks from his repository and removes ebooks which are not related to infosec. So we are making it easy for infosec folks to grab an ebook of their wish. We also recommend you to buy a hardcopy of the freely downloaded ebook to give credits to the respective authors for their effort in writing the book and PacktPub for publishing it.

**2. License ?**

This repository will *never* have a license. Credits for each book goes to the respective authors and also the Packt Publications.

**3. Why are you doing this ? What is Hack with Github ?**

All our focus is on open source security. So Hack with Github - in short - is an initiative to showcase open source tools on GitHub related to infosec - hacking, malware analysis, and much more.
If you like to get more feeds on such open source security tools:
- Follow [@HackwithGithub](https://twitter.com/HackwithGithub) on Twitter
- Follow [@HackwithGithub](https://www.facebook.com/HackWithGithub/) on Facebook
- Follow [@hackwith](https://github.com/hackwith) on GitHub to get notifications of the repos we star

**Feedback / comments:** shoot a mail to hackwithgithub at gMaiL DoT CoM
